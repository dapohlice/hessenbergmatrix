import numpy as np


def rot_cos(a, b):
    """
    Gibt den Cosinus der Rotationsmatrix
    :param a: a_(j+1,1)
    :param b: a_(i,j)
    :return: cosinus omega
    """

    cos_t = np.absolute(a)
    p_a = np.power(a, 2)
    p_b = np.power(b, 2)


    cos_b = np.sqrt(p_a + p_b)

    return cos_t / cos_b


def rot_sin(a, b):
    """
    Gibt den Sinus der Rotationsmatrix
    :param a: a_(j+1,1)
    :param b: a_(i,j)
    :return: sinus omega
    """

    sin_t = np.sign(a) * b
    sin_b = np.sqrt(np.power(a, 2) + np.power(b, 2))
    return sin_t / sin_b


def create_rot_matrix(size, p, q, sin, cos):
    """
    Erstellt die Rotation Matrix size x size
    :param size: größe der Matrix
    :param p: Position p
    :param q: Position q
    :param sin: Sinus-Wert
    :param cos: Cosinus-Wert
    :return: RotationMatrix
    """

    "Temporäre Matrix als Liste"
    m = []
    for x in range(0, size):
        n = []
        for y in range(0, size):
            if x == y:
                if x != p and x != q:
                    n.append(1)
                else:
                    n.append(cos)
            elif x == p and y == q:
                n.append(sin * -1)
            elif x == q and y == p:
                n.append(sin)
            else:
                n.append(0)
        m.append(n)

    matrix = np.array(m)
    return matrix


def get_rotation_matrix(matrix, i, j):
    """
    Erstellt die Rotationmatrix
    :param matrix: Ausgangsmatrix
    :param i: Spalte
    :param j: Zeile
    :return:Rotationsmatrix
    """

    a = matrix.item((i, j))
    b = matrix.item((j+1, j))

    cos = rot_cos(b, a)
    sin = rot_sin(b, a)

    angle = np.degrees(np.arccos(cos)).round(5)
    print("Rotationswinkel: "+str(angle))

    return create_rot_matrix(4, j+1, i, sin, cos)


def rotate_matrix(matrix, i, j):
    """
    Rotiert eine Matrix
    :param matrix: Ausgangsmatrix
    :param i: Spalte
    :param j: Zeile
    :return: rotierte Matrix
    """

    rot_matrix = get_rotation_matrix(matrix, i, j)

    print("Rotations Matrix:")
    print(rot_matrix)

    result = rot_matrix.dot(matrix).dot(rot_matrix.transpose())

    return result


def recursive_rotation(matrix):
    """
    Rotiert die gegebene Matrix so, dass sie am Ende einer Hessenberg-Matrix entspricht
    :param matrix: Matrix
    :return: Hessenberg-Matrix
    """
    s = matrix.shape[0]

    rot_matrix_list = []

    for j in range(0, s-1):
        for i in range(2+j, s):

            print("_____Rotation für___i: "+str(i)+"__j: "+str(j)+"_________________________________")

            rot_matrix = get_rotation_matrix(matrix, i, j)
            rot_matrix_list.append(rot_matrix)
            print("Rotations Matrix:")
            print(rot_matrix)

            transformed_matrix = rot_matrix.transpose()

            matrix = transformed_matrix.dot(matrix).dot(rot_matrix)

    print("_____TEST______________________________________________________")

    test_matrix = matrix

    for u in range(len(rot_matrix_list)-1, -1, -1):
        rotation_matrix = rot_matrix_list[u]
        test_matrix = rotation_matrix.dot(test_matrix).dot(rotation_matrix.transpose())

    print(test_matrix)

    return matrix
