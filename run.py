

import general as g
import numpy as np
import sys
import file

input_path = None
output_path = None

argv_length = len(sys.argv)

for i in range(1, argv_length):
    arg = sys.argv[i]
    if arg == "help" or arg == "-help" or arg == "-h":
        print("___Transformation auf Hessenberg-From__________________________")
        print("    Transformiert eine Matrix auf Hessenbergform")
        print("    Autor: Alexander Pohl")
        print("")
        print("Kommandos:")
        print("  help")
        print("    help | -help | -h")
        print("      Zeigt diesen Hilftext")
        print("  input <Datei-Pfad>")
        print("    input | -i")
        print("     lädt die Ausgangsmatrix aus der Datei")
        print("     die Matrix muss in der Datei pro Zeile gespeichert sein und die einzelnen Werte müssen mit Komma getrennt sein")
        print("     z.B.")
        print("       1.0,2.0")
        print("       2.0,1.0")
        print("  output <Datei-Pfad")
        print("    output | -o")
        print("     speichert die Transformtierte Matrix in eine Datei")
        print("")
        sys.exit()
    elif arg.startswith("-i"):
        i = i+1
        if i >= argv_length:
            print("Eingabe-Dateipfad nicht gegeben")
            sys.exit()
        input_path = sys.argv[i]
    elif arg.startswith("input"):
        i = i+1
        if i >= argv_length:
            print("Eingabe-Dateipfad nicht gegeben")
            sys.exit()
        input_path = sys.argv[i]
    elif arg.startswith("-o"):
        i = i + 1
        if i >= argv_length:
            print("Ausgabe-Dateipfad nicht gegeben")
            sys.exit()
        output_path = sys.argv[i]
    elif arg.startswith("output"):
        i = i + 1
        if i >= argv_length:
            print("Ausgabe-Dateipfad nicht gegeben")
            sys.exit()
        output_path = sys.argv[i]

print("_____MATRIX____________________________________________________")

if input_path is not None:
    matrix = file.read_matrix_from_file(input_path)
    print("Geladen aus: "+input_path)
else:
    matrix = np.array(
        [
            [1.0, 2.0,  3.0,  4.0],
            [5.0, 6.0,  7.0,  6.0],
            [8.0, 3.0, 11.0,  12.0],
            [12.0, 3.0,  6.0,  7.0]
        ]
    )

print(matrix)

rotated_matrix = g.recursive_rotation(matrix).round(6)
print("_____ERGEBNIS__________________________________________________")
print(rotated_matrix)

if output_path is not None:
    file.write_matrix_to_file(rotated_matrix,output_path)
    print("Ergebnis in "+output_path+" gespeichert.")
