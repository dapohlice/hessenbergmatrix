from io import StringIO
import numpy as np
import sys

def read_matrix_from_file(path):
    """
    Ließt eine Matrix aus einer Datei
    :param path: Dateipfad
    :return: Matrix
    """
    try:
        matrix = np.genfromtxt(path, delimiter=",")
    except:
        print("Datei "+path+" konnte nicht geladen werden")
        sys.exit()
    return matrix


def write_matrix_to_file(matrix, path):
    """
    Speichert eine Matrix in eine Datei
    :param matrix: Matrix
    :param path: Dateipfad
    """
    try:
        np.savetxt(path, matrix, delimiter=",", fmt="%.5f")
    except:
        print("Matrix konnte nicht in " + path + " gespeuchert werden")