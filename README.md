# README

## Umgebung
  - zur Ausführung wird Python benötigt
  - Bibliotheken:
    - numpy
### Methode A: Manuel Installieren
  - numpy auf dem lokalen PC installieren
### Methode B: pipenv
  Benötigt: 
    pipenv 
     
  - im Projektverzeichnis
  - installieren von Numpy
  ```bash
  pipenv install
  ```
  - ausführen
  ```bash
  pipenv run python run.py
  ```
## Benutzung
  ```bash
      python run.py
  ```
Beispiel:

  ```bash
    python run.py -i matrix.txt -o transponierte-matrix.txt
  ```
  
### Hilfe:
  ```bash
      ./run.py help 
  ```
### Matrix Eingeben:
  ```bash
      ./run.py -i <Datei-Pfad>
  ```
### Matrix Ausgeben:
  ```bash
      ./run.py -o <Datei-Pfad>
  ```